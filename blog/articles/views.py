from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, logout
from django.contrib.auth import login as deflogin
from time import sleep

# Create your views here.
from articles.models import Article

def archive(request):
	return render(request, 'archive.html', {"posts": Article.objects.all()})

def get_article(request, article_id):
	try:
		post = Article.objects.get(id=article_id)
		return render(request, 'article.html', {"post": post})
	except Article.DoesNotExist:
		raise Http404

def create_post(request):
	if request.user.is_anonymous:
		raise Http404
	if request.method == "GET":
		return render(request, 'addform.html', {})
	# обработать данные формы, если метод POST
	form = {
		'text': request.POST["text"], 'title': request.POST["title"]
	}
	# в словаре form будет храниться информация, введенная пользователем
	if form["text"] and form["title"]:
		try:
			Article.objects.get(title=form['title'])
		except Article.DoesNotExist:
			# если поля заполнены без ошибок
			article = Article.objects.create(text=form["text"], title=form["title"], author=request.user)
			return redirect('get_article', article_id=article.id)
			# перейти на страницу поста
		else:
			form['errors'] = 'An article with the same name exists, choose another one, please!'
			return render(request, 'addform.html', {'form': form})
	else:
			# если введенные данные некорректны
		form['errors'] = u"Не все поля заполнены"
		return render(request, 'addform.html', {'form': form})

def register(request):
	if request.method == 'GET':
		return render(request, 'userform.html',{})

	form = {
		'login':request.POST['login'],
		'email':request.POST['email'],
		'password':request.POST['password'],
	}
	username, email, password = form['login'], form['email'], form['password']
	if not username or not email or not password: 
		form['errors'] = 'Blank fields are not allowed!'
		return render(request, 'userform.html', {'form': form})
	try:
		User.objects.get(username=username)
		form['errors'] = 'This login is occupied, choose another one, please!'
		return render(request, 'userform.html', {'form': form})
	except User.DoesNotExist:
		try:
			User.objects.get(email=email)
			form['errors'] = 'There is a user with this email, choose another one, please!'
			return render(request, 'userform.html', {'form': form})
		except User.DoesNotExist:
			User.objects.create_user(username, email, password)
			return redirect('/')

def login(request):
	if request.method == 'GET':
		return render(request, 'loginform.html',{})

	form = {
		'login':request.POST['login'],
		'password':request.POST['password'],
	}
	username, password = form['login'], form['password']
	try:
		User.objects.get(username=username)
	except User.DoesNotExist:
		try:
			User.objects.get(email=username)
			form['errors'] = 'Enter your login instead of e-mail, please.'
			return render(request, 'loginform.html', {'form': form})
		except User.DoesNotExist:
			form['errors'] = 'THis login is neither username nor e-mail, please check your input!'
			return render(request, 'loginform.html', {'form': form})

	user = authenticate(username=username, password=password)
	if user is None:
		form['errors'] = 'Your password is incorrect, try again, please.'
		return render(request, 'loginform.html', {'form': form})
	else:
		deflogin(request, user)
		return redirect('/')

def logout_view(request):
	logout(request)
	return redirect('/')

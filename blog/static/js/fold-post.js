var coll = document.getElementsByClassName("fold-button");
var i;

for (i = 0; i < coll.length; i++) {
	coll[i].addEventListener("click", function() {
		this.classList.toggle("active");
	    var onepost = this.nextElementSibling;
	    var showbutton = onepost.nextElementSibling;
	    if (onepost.style.display === "block") {
	     	onepost.style.display = "none";
	     	this.innerHTML = "Show"
	    }
	    else { 
	    	onepost.style.display = "block";
	    	this.innerHTML = "Collapse"
	    }
  	});
}
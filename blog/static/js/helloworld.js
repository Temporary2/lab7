var groupmates = [
    {
        "name": "Александр",
        "surname": "Иванов",
        "group": "БВТ1702",
        "marks": [4, 3, 4]
    },
    {
        "name": "Иван",
        "surname": "Петров",
        "group": "БСТ1702",
        "marks": [4, 4, 4]
    },
    {
        "name": "Кирилл",
        "surname": "Смирнов",
        "group": "БАП1801",
        "marks": [5, 5, 5]
    }
];
console.log(groupmates);

var rpad = function(str, length) {
    str = str.toString(); // преобразование в строку
    while (str.length < length)
        str = str + ' '; // добавление пробела в конец строки
    return(str);
};

var printStudents = function(students) { 
    console.log(
        rpad("Имя", 15),
        rpad("Фамилия", 15),
        rpad("Группа", 8),
        rpad("Оценки", 20)
    );
    for (var i = 0; i<=students.length-1; i++) {
        // в цикле выводится каждый экземпляр студента 
        console.log(
            rpad(students[i]['name'], 15),
            rpad(students[i]['surname'], 15),
            rpad(students[i]['group'], 8),
            rpad(students[i]['marks'], 20)
            );
    };
    console.log('\n');
};

printStudents(groupmates); 

var sortStudentsGroup = function(students) { 
    var group = prompt('Which group to show?', '');
    console.log(
        rpad("Имя", 15),
        rpad("Фамилия", 15),
        rpad("Группа", 8),
        rpad("Оценки", 20)
    );
    for (var i = 0; i<=students.length-1; i++) {
        // в цикле выводится каждый экземпляр студента 
        if (students[i]['group'] == group)
            console.log(
            rpad(students[i]['name'], 15),
            rpad(students[i]['surname'], 15),
            rpad(students[i]['group'], 8),
            rpad(students[i]['marks'], 20)
            );
    };
    console.log('\n');
};

var averageMark = function(s) {
    var sum = 0;
    for (var i = 0; i<=s.length-1; i++) 
        sum += s[i]
    return(sum/s.length);
};
var sortStudentsAverage = function(students) { 
    var ave = parseFloat(prompt('Enter least average mark:', ''));
    console.log(
        rpad("Имя", 15),
        rpad("Фамилия", 15),
        rpad("Группа", 8),
        rpad("Оценки", 20)
    );
    for (var i = 0; i<=students.length-1; i++) {
        // в цикле выводится каждый экземпляр студента 
        if (averageMark(students[i]['marks']) > ave)
            console.log(
            rpad(students[i]['name'], 15),
            rpad(students[i]['surname'], 15),
            rpad(students[i]['group'], 8),
            rpad(students[i]['marks'], 20)
            );
    };
    console.log('\n');
};

sortStudentsGroup(groupmates);
sortStudentsAverage(groupmates);
